import { Router } from 'express'
import multer from 'multer'

import isConnected from '../../middlewares/is-connected.js'
import passwordConfirmation from '../../middlewares/password-confirmation.js'
import { tryTo, emptyError } from '../../middlewares/errors.js'
import ArticlePolicy from '../../policies/article.policy.js'
import SubTopicPolicy from '../../policies/subTopic.policy.js'

import getArticles from './ctrl/get.articles.js'
import getArticle from './ctrl/get.article.js'
import getArticleKeyword from './ctrl/get.article.keywords.js'
import postArticle from './ctrl/post.article.js'
import postArticleImage from './ctrl/post.article.image.js'
import putArticle from './ctrl/put.article.js'
// import putArticleKeyword from './ctrl/put.article.keywords.js'
import deleteArticle from './ctrl/delete.article.js'

function fileFilter (req, file, cb) {
  const allowedTypes = ['image/jpeg', 'image/png']

  if (!allowedTypes.includes(file.mimetype)) {
    const error = new Error('Wrong file type !')
    error.code = 'LIMIT_FILE_TYPES'
    return cb(error, false)
  }

  cb(null, true)
}

const upload = multer({
  dest: './temp',
  fileFilter,
  limits: {
    fileSize: 20000000 // 20Mo
  }
})

const {
  canGetUniverseIndirect,
  canEditUniverseIndirect
} = require('../../middlewares/access-rights.js')

const canGet = canGetUniverseIndirect(ArticlePolicy.getUniverseId, 'id', 'params')
const canAdd = canEditUniverseIndirect(SubTopicPolicy.getUniverseId, 'idSubTopic', 'body')
const canEdit = canEditUniverseIndirect(ArticlePolicy.getUniverseId, 'id', 'params')

const router = Router()

// Get
router.get('/', tryTo(getArticles, emptyError))
router.get('/:id', canGet, tryTo(getArticle, emptyError))
router.get('/:id/keywords', canGet, tryTo(getArticleKeyword, emptyError))

// Post
router.post('/', isConnected, canAdd, tryTo(postArticle, emptyError))
router.post('/:id', upload.single('article-image'), tryTo(postArticleImage, emptyError))

// Put
router.put('/:id', isConnected, canEdit, tryTo(putArticle, emptyError))

// Delete
router.delete('/:id', isConnected, canEdit, tryTo(deleteArticle, emptyError))

export default router
