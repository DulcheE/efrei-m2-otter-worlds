import { Router } from 'express'
import multer from 'multer'

import isConnected from '../../middlewares/is-connected.js'
import passwordConfirmation from '../../middlewares/password-confirmation.js'
import { tryTo, emptyError } from '../../middlewares/errors.js'

import getUsers from './ctrl/get.users.js'
import getUser from './ctrl/get.user.js'
import getUserCharacters from './ctrl/get.user.characters'
import getUserGroups from './ctrl/get.user.groups'
import getUserUniverses from './ctrl/get.user.universes'
import getUserUniversesPlays from './ctrl/get.user.universesPlays'
import postUser from './ctrl/post.user.js'
import postUserImage from './ctrl/post.user.image.js'
import deleteUser from './ctrl/delete.user.js'

function fileFilter (req, file, cb) {
  const allowedTypes = ['image/jpeg', 'image/png']

  if (!allowedTypes.includes(file.mimetype)) {
    const error = new Error('Wrong file type !')
    error.code = 'LIMIT_FILE_TYPES'
    return cb(error, false)
  }

  cb(null, true)
}

const upload = multer({
  dest: './temp',
  fileFilter,
  limits: {
    fileSize: 20000000 // 20Mo
  }
})

const {
  canGetUniverse,
  isUser
} = require('../../middlewares/access-rights.js')

const router = Router()

// Get
router.get('/', tryTo(getUsers, emptyError)) // no policy
router.get('/:id', tryTo(getUser, emptyError)) // no policy
router.get('/:id/characters', tryTo(getUserCharacters, emptyError)) // can see private universes
router.get('/:id/groups', canGetUniverse('universe', 'query'), tryTo(getUserGroups, emptyError)) // can see universe
router.get('/:id/universes', tryTo(getUserUniverses, emptyError)) // can see private universes
router.get('/:id/universes-plays', tryTo(getUserUniversesPlays, emptyError)) // can see private universes

// Post
router.post('/', tryTo(postUser, emptyError))
router.post('/:id', upload.single('user-image'), tryTo(postUserImage, emptyError))

// Delete
router.delete('/:id', isConnected, isUser('id', 'params'), tryTo(deleteUser, emptyError))

export default router
